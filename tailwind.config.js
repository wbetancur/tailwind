/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./public/**/*.{html,js}"],
  theme: {
    extend: {
      container: {
        center: true,
        screens: {
          bigMobile: '420px',
          tablet: '768px',
          desktop: '1024px',
        },
      },
      colors: {
        primary: '#fe3',
        secondary: '#ba0',
      },
    },
  },
  plugins: [],
}


